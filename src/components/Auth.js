import React from "react";
import PropTypes from "prop-types";
import firebase from "firebase";
import Login from "./Login";
import base, { firebaseApp } from "../base";

class Auth extends React.Component{

	authHandler = async(authData) => {
		//retrieving data from firebase
		//const data = await base.fetch()
		console.log(authData);
	};

	authenticate = provider => {
		//Only working with FB rn
		//Have to find a way to retrieve which login was used
		const authProvider = new firebase.auth.FacebookAuthProvider();
		firebaseApp
		.auth()
		.signInWithPopUp(authProvider)
		//When someone comes back from sign in, it runs the .then() function
		.then(this.authHandler)
	};

	render(){
		return <Login authenticate={this.authenticate} />;
	}
}
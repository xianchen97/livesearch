import Rebase from "re-base";
import firebase from "firebase";

const firebaseApp = firebase.initializeApp({
 	apiKey: "AIzaSyACOIa05Vqr8mRxSgTFt5H4P7gL3iiTLTE",
    authDomain: "idea-4104b.firebaseapp.com",
    databaseURL: "https://idea-4104b.firebaseio.com",
});

const base = Rebase.createClass(firebaseApp.database());

// Named export
export { firebaseApp };

export default base;
import React from 'react';
import PropTypes from 'prop-types';

const Login = props => (
	<nav className="login">
		<h2> Login </h2>
		<p> Sign in. </p>
		<button className="facebook" 
		onclick = {() => props.authenticate('Facebook')}> 
		Log in with Facebook
		</button>
		<button className="twitter" 
		onclick = {() => props.authenticate('Facebook')}> 
		Log in with Twitter
		</button>
	</nav>
);

Login.propTypes = {
	authenticate: PropType.func.isRequired
}

export default Login;
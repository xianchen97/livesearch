import React from 'react';

class Offer extends React.Component{
		render(){
			return(
				<div id="offerModal" class="modal">
				   <div class="modal-content">
				      <form>
				         <h1>
				            OFFER ITEM	
				         </h1>
				         <hr />
				         <h2>
				            Selected Item
				         </h2>
				         <div>
				            <select id="mySelection" name="mySelection">
				               <option value="item1">Item 1</option>
				               <option value="item2">Item 2</option>
				               <option value="item3">Item 3</option>
				            </select>
				         </div>
				         <hr />
				         <h3>
				            Send a message
				         </h3>
				      <form name="form">
				         <input type="text" id="inputMessage" name="inputMessage" />
				         <hr />
				         <div class="offerBtn">
				            <button class="sendEmail" onClick="messaging()" data-dismiss="modal">OFFER</button>
				         </div>
				         <div id="my_div"></div>
				      </form>
				      </form>
				   </div>
				</div>
			);
		}
}

export default Offer;
import React from 'react';	
import ModalOffer from './ModalOffer';

class ItemField extends React.Component{
	render(){
		return(
		    <div id="borrow-div">
		      <table class="borrower-table">
		        <tr id="top-sec">
		          <td id="prof-pic" width="100"><img class="img-prof" src="dog-man.jpg" /></td>
		          <td id="product">item name</td>
		          <td>
		            <div id="offer">
		              <ModalOffer />
		            </div>
		          </td>
		        </tr>
		        <tr id="bottom-sec">
		          <td class="search-info">12:20AM</td>
		          <td class="search-info">3 days ago</td>
		          <td class="search-info">Vancouver, BC</td>
		        </tr>
		      </table>
		    </div>
		);
	}
}

export default ItemField;
import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import NotFound from "./NotFound";
import App from "./App";
import Offer from "./Offer";
import ModalOffer from "./ModalOffer";


const Router = () => (
	<BrowserRouter>
		<Switch>
				<Route exact path="/" component={ App } />
				<Route path="/Offer" component= { Offer } />
				<Route path="/ModalTester" component = { ModalOffer } />		
				<Route component={ NotFound } />
		</Switch>
	</BrowserRouter>
);

export default Router;
import React from 'react';
import ReactModal from 'react-modal';
import Offer from './Offer';
import '../css/ModalOffer.css';

const customStyles = {
	content : {
	    'background-color': '#fefefe',
	    margin: 'auto',
	    padding: '20px',
	    border: '1px solid #888',
	    width: '40%'
	}
}

class ModalOffer extends React.Component {
  constructor () {
    super();
    this.state = {
      showModal: false
    };
    
    this.handleOpenModal = this.handleOpenModal.bind(this);
    this.handleCloseModal = this.handleCloseModal.bind(this);

  }
  
  handleOpenModal () {
    this.setState({ showModal: true });
  }
  
  handleCloseModal () {
    this.setState({ showModal: false });
  }
  
  render () {
    return (
      <div>
        <button onClick={this.handleOpenModal}>Offer</button>
        <ReactModal 
           id = "modalView"
           isOpen={this.state.showModal}
           contentLabel="Minimal Modal Example"
           style = {customStyles}
        >
        	      <button onClick={this.handleCloseModal}>&times;</button>

          <Offer />
        </ReactModal>
      </div>
    );
  }
}

export default ModalOffer;
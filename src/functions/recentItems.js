ref.orderByChild("time").on("child_added", function(snapshot){

	let date = new Date(snapshot.val().time * 1000);
	let months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
	let month = months[date.getMonth()];
	let day = date.getDay();
	let hours = date.getHours();
	let minutes = "0" + date.getMinutes();
	let seconds = "0" + date.getSeconds();
	let formattedTime = hours + ":" + minutes.substr(-2) + ":" + seconds.substr(-2);

	make_list(snapshot.val().ser + " was searched on "
		+ month + " " + day + " at " + formattedTime + " by " + snapshot.val().user);

	return snapshot.val().ser;

	function make_list(string){
		let list = document.getElementById("list");
		if(list.children.length > 2){
			list.children[0].innerHTML = list.children[1].innerHTML;
			list.children[1].innerHTML = list.children[2].innerHTML;
			list.children[2].innerHTML = "<li>"+string+"</li>";
		}
		else{
			list.innerHTML += "<li>"+string+"</li>";
		}
	}

});